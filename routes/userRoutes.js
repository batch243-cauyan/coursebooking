const express = require("express");
const router = express.Router();
const auth = require("../auth")

const userController = require("../controllers/userControllers")
	
	// Route for checking Email
	router.post("/checkEmail", userController.checkEmailExists);
	// route for registration
	router.post("/register",  userController.checkEmailExists,userController.registerUser);
	// route for log in
	router.post("/login", userController.loginUser);

	router.post("/details", auth.verify ,userController.getProfile);

	router.get("/profile", userController.profileDetails);

	router.get("/", userController.getAllUsers);
	
	// Update role
	router.patch("/updateRole/:userId", auth.verify, userController.updateRole);

	// enrollment
	router.post("/enroll/:courseId", auth.verify, userController.enroll);


module.exports = router;